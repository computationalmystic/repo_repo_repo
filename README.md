<div align="center">

# Bravo-OSS
### Enhancing Security in Software Development

</div>

---

## 🚨 Problem Statement

<div align="center">

<h2> **Not enough repositories adhere to DevSecOps best practices.**<br/>
This results in leaked secrets, compromised keys, and increased security risks.</h2>

</div>

- [1 GSA Key Found](https://forager.trufflesecurity.com/secret/541902)
- [1 army.mil Key Found](https://forager.trufflesecurity.com/secret/753516)

---

## 👥 Team Members

- **Remy DeCausemaker** - Open Source Lead, Digital Service at CMS.gov, Use Case Owner
- **Mark Terry** - MSgt, Supra Coder / Cyber Ops - System Ops, Space Force
- **Armando Cabrera** - 2d Lt, Cyber Effects Officer, Space Force
- **Vahe Martirosyan** - Specialist 3(Spc3), Cyber Defense Operator / Cyber Tactician, Space Force

---

## 🖼️ Visuals

| ![Bravo OSS Animated](output.gif "Bravo OSS Animated") | ![Bravo OSS Static](output.webp "Bravo OSS Static") |
|:---------------------------------------------------------------:|:-------------------------------------------------------------:|
|                           *Animated*                            |                           *Static*                            |

*Animated and Static Representations of Our Mission*

---


## 📍 Team Location

- **Room BRAVO 249**
- [Repo on GitLab](https://gitlab.com/bravo-oss/repo_repo_repo)

---

## 📝 Summary

Our mission at *Bravo-oss* is to integrate DevSecOps best practices into software development, enhancing security and reducing continuity risks across repositories.

---

## 🎯 Objectives

### Problem Trying to Solve

We aim to improve security and quality in .gov projects hosted on GitLab through gitlab runners, python scripts, and Jupyter notebooks.

### Approach Taken

- **Secrets Scanning** via issue filing.
- **Static Code Analysis** via issue filing.
- **Software Bill of Materials** generation and issue filing.
- **Fosstars metrics report** and issue filing.
- **OpenSSF Score Card** generation and issue filing.
- **Foreign Character Detection** from commit authors.
- **User Trust Ratings** from repo developers. 
- **Community Health Metric** of the repo.
- **Common issue visualizer** of the required dependencies.

### Future Goals

Visit our [ISSUES page](https://gitlab.com/bravo-oss/repo_repo_repo/-/issues) for developments and ideas we're exploring.

---

## 📈 Stakeholders

- **1st Order**: Developers and repository owners.
- **2nd Order**: Project, product, and development organization managers.
- **3rd Order**: CISOs, CIOs, and organizational leaders.

---
