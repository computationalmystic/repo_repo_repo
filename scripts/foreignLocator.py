#!/usr/bin/env python
# coding: utf-8

import gitlab
import re
import argparse

def contains_russian_or_chinese(text):
    # Regular expression pattern for Russian and Chinese characters
    russian_pattern = re.compile(r'[а-яА-ЯёЁ]')
    chinese_pattern = re.compile(r'[\u4e00-\u9fff]')

    # Check if the text contains Russian or Chinese characters
    contains_russian = bool(re.search(russian_pattern, text))
    contains_chinese = bool(re.search(chinese_pattern, text))

    return contains_russian or contains_chinese

pattern = re.compile(r'\b(?:russia|china)\b', flags=re.IGNORECASE)

parser = argparse.ArgumentParser(description='Process GitLab repository information.')
parser.add_argument ('--projectID',  help='Project ID')
parser.add_argument ('--apiToken', help="API TOKEN")
args = parser.parse_args()

# GitLab credentials
gitlab_token = args.apiToken
repo_url = args.projectID

# Connect to GitLab
gl = gitlab.Gitlab('https://gitlab.com', private_token=gitlab_token)

project = gl.projects.get(repo_url)
commits = project.commits.list(get_all=True)

emails = []
commitIds = []
endings = [".ru", ".cn"]
for oneCommit in commits:
    commit = project.commits.get(oneCommit.id)
    author_email = commit.author_email
    ids = commit.short_id
    for ending in endings:
        if author_email.endswith(ending):
            emails.append(author_email)
            commitIds.append(ids)


projectUsers = project.users.list(get_all=True)
threatUsers = []

for projectUser in projectUsers:
    user = gl.users.get(projectUser.id)
    if(user.bot != True):
        if(contains_russian_or_chinese(user.bio) or contains_russian_or_chinese(user.name) or pattern.search(user.location)):
            threatUsers.append(f"**Username:** {user.username}, **Name:** {user.name}, **Bio:** {user.bio}, **Location:** {user.location}, **UserID:** {user.id}")
        else:
            pass


# Create issue
project = gl.projects.get(repo_url)

# Concatenate emails and commit IDs into a single string with new lines
email_commit_info = "\n".join([f"**Email**: {email}, **Commit ID**: {commit_id}  \n" for email, commit_id in zip(emails, commitIds)])

# Create a single issue with all detections
issue_title = f"Security Wawrning: Possible adversarial email detected in previous commits"
issue_description = f"**Description**: We have detected potential adversarial committers in previous commits. Here are the details:  \n\n{email_commit_info}"
issue_description += "  \n".join(threatUsers)
issue = project.issues.create({'title': issue_title,
                                'description': issue_description})




if issue:
    print("Issue created successfully!")
else:
    print("Failed to create issue.")





