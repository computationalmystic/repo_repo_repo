#!/usr/bin/env python
# coding: utf-8

import gitlab
import json 
import argparse

parser = argparse.ArgumentParser(description='Process GitLab repository information.')
parser.add_argument ('--projectID',  help='Project ID')
parser.add_argument ('--apiToken', help="API TOKEN")
args = parser.parse_args()

# GitLab credentials
gitlab_token = args.apiToken
repo_url = args.projectID

file_name = 'gl-sast-report.json'
file = open(file_name, 'rb')
data = json.load(file)
print(data)


# Connect to GitLab
gl = gitlab.Gitlab('https://gitlab.com', private_token=gitlab_token)

project = gl.projects.get(repo_url)

name = []
description = []
severity = []
raw = []
location = []

if(data["vulnerabilities"] == []):
    pass
else:
    for i in data['vulnerabilities']:
        name.append(i['name'])
        description.append(i["description"])
        severity.append(i['severity'])
        raw.append(i["raw_source_code_extract"])
        location.append(i["location"]['file'])


issue_commit_info = "\n".join([f"**Security Vulnerability**: {name}  \n , **Description**: {description}  \n , **Severity**: {severity}  \n, **Raw Source Code Extract**: {raw}  \n ,**Location**: {location}" for name, description, severity, raw, location in zip(name, description, severity, raw, location)])


# Create issue
if(data["vulnerabilities"] == []):
    pass
else:
    project = gl.projects.get(repo_url)
    issue_title = f"SAST Report"
    issue_description = f"**Description**: SAST Vulnerabilities. Here are the details:  \n\n{issue_commit_info}"
    issue = project.issues.create({'title': issue_title,
                                'description': issue_description})



if issue:
    print("Issue created successfully!")
else:
    print("Failed to create issue.")
