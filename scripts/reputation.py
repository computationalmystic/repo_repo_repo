#!/usr/bin/env python
# coding: utf-8

import os
import gitlab
import argparse


parser = argparse.ArgumentParser(description='Process GitLab repository information.')
parser.add_argument ('--projectID',  help='Project ID')
parser.add_argument ('--apiToken', help="API TOKEN")
args = parser.parse_args()

# GitLab credentials
gitlab_token = args.apiToken
repo_url = args.projectID
reports = []

# Connect to GitLab
gl = gitlab.Gitlab('https://gitlab.com', private_token=gitlab_token)

project = gl.projects.get(repo_url)

projectUsers = project.users.list(get_all=True)
for projectUser in projectUsers:
    ids = projectUser.id
    # Get the user
    user = gl.users.get(ids)
    # Fetch projects owned by the user
    user_projects = user.projects.list(get_all=True)

    # Initialize a variable to store the total stars
    total_stars = 0
    total_forks = 0
    locExists = False
    bioExists = False
    companyExists = False
    repoCount = 0
    repoDescription = []

    bioRating = 0
    user_popularity = 0
    repo_popularity = 0

    # Iterate through each project and sum up the stars
    for project in user_projects:
        repoCount += 1
        currentProject = gl.projects.get(project.id)
        total_stars += currentProject.star_count
        if hasattr(currentProject, 'forks_count'):
            total_forks += currentProject.forks_count
        else:
            total_forks = 0
        repoDescription.append(currentProject.description)
    if user.location:
        locExists = True
    if user.bio:
        bioExists = True
    if user.work_information:
        companyExists = True



    if bioExists:
        word_count = user.bio
        words = word_count.split()
        numwords = len(words)
        res = numwords * 10
        bioRating = 100 if res > 100 else res

    if total_stars > 0:
        STAR_RATE = total_stars + repoCount / repoCount

        rate = user.followers / repoCount + STAR_RATE
        res = min(int(rate * 15), 100)
        user_popularity = 100 if res > 100 else res


    if total_stars > 0 and total_forks > 0:

        rate = (total_stars + total_forks * 1.2) / repoCount
        res = min(int(rate * 16), 100)
        repo_popularity = 100 if res > 100 else res

    if repoDescription != None:
        repoDescriptionLength = []
        rate = []
        for description in repoDescription:
            if(description != None):
                repoDescriptionSplit = description.split()
            else:
                repoDescriptionSplit = []
            repoDescriptionLength = len(repoDescriptionSplit)
            if repoDescriptionLength == 0:
                repoDescriptionLength = 1
            rate.append(repoCount / repoDescriptionLength)
        # Calculate the average rate
        if(len(rate) == 0):
            rateLen = 1
        else:
            rateLen = len(rate)
        if(sum(rate) == 0):
            rateSum = 1
        else:
            rateSum = sum(rate)
        avg_rate = rateSum / rateLen
        
        # Calculate the rating
        res = min(int(100 / avg_rate), 100)
        if(avg_rate == 1):
            res = 0
        repo_description_rating = 100 if res > 100 else res
    CombinedScore = (repo_description_rating + repo_popularity + user_popularity + bioRating) / 4
    reports.append("**" + user.username + " (" + user.web_url + ")**" + " has a trust score of " + str(CombinedScore))

# Create issue
project = gl.projects.get(repo_url)

# Create a single issue with all detections
issue_title = f"Reputation Scores: Repo users reputations"
issue_description = f"**Description**: The following are estimated reputation points based on repo user's activities:  \n"

# Add each report to the issue description
for report in reports:
    issue_description += report + "  \n"

issue = project.issues.create({'title': issue_title,
                                'description': issue_description})


if issue:
    print("Issue created successfully!")
else:
    print("Failed to create issue.")




