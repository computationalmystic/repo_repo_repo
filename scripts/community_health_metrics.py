import gitlab
import argparse

import warnings
warnings.filterwarnings('ignore')

parser = argparse.ArgumentParser(description='Process GitLab repository information.')
parser.add_argument ('--url', help="Input URL")
parser.add_argument ('--apiToken', help="API TOKEN")
args = parser.parse_args()

# GitLab credentials
input_variable = args.url
token = args.apiToken

split = input_variable.split("/")[3:]
project = "/".join(split)

# project = input_variable.split(".com/")[1]

gl = gitlab.Gitlab('https://gitlab.com', private_token=token)

# Replace 'namespace/project' with your project's path
project_id = project
project = gl.projects.get(project_id)

print("*******Community Health Metrics*******")

def safe_execute(callable_func, *args, **kwargs):
    """
    Executes a callable safely, catching 403 Forbidden errors and returning a fallback value.
    """
    try:
        return callable_func(*args, **kwargs)
    except gitlab.exceptions.GitlabHttpError as e:
        if e.response_code == 403:
            return "n/a"
        else:
            raise

# Assuming 'project' is already defined and 'gitlab' has been imported

# Safely fetch project creation date parts
created_at = safe_execute(lambda: project.created_at)
year, month, date, time = ("n/a", "n/a", "n/a", "n/a") if created_at == "n/a" else (
    created_at.split("-")[0],
    created_at.split("-")[1],
    created_at.split("-")[2][0:2],
    created_at.split("-")[2][3:11],
)

# Safely fetch commits
commits = safe_execute(lambda: project.commits.list(all=True)) or []

# Similar approach for issues and merge requests
open_issues = safe_execute(lambda: project.issues.list(state='opened', all=True)) or []
num_open_issues = len(open_issues) if open_issues != "n/a" else "n/a"

closed_issues = safe_execute(lambda: project.issues.list(state='closed', all=True)) or []
num_closed_issues = len(closed_issues) if closed_issues != "n/a" else "n/a"

mrs = safe_execute(lambda: project.mergerequests.list(state='opened')) or []

closed_mrs = safe_execute(lambda: project.mergerequests.list(state='closed', all=True)) or []
num_closed_mrs = len(closed_mrs) if closed_mrs != "n/a" else "n/a"

try:
    # Attempt to fetch audit events and calculate their count
    num_audits = len(project.audit_events.list())
except Exception as e:
    # If an error occurs, set num_audits to 'na'
    num_audits = 'na'


# Process commits for unique authors and emails
authors = set()
emails = set()
if commits != "n/a":
    for commit in commits:
        authors.add(commit.author_name)
        emails.add(commit.author_email)

num_authors = len(authors) if authors else "n/a"
num_emails = len(emails) if emails else "n/a"

### Printing Jobs

# project
print(f"Project Web Url: {(project.web_url)}")
#url read me
print(f"Project Url Readme: {(project.readme_url)}")
print('Project Creation: '+month +"-"+ date+"-"+ year+","+time)
# security and compliance
print(f"Security and Compliance enable: {(project.security_and_compliance_enabled)}")
# merge request
print(f"Merge request: {(project.merge_requests_access_level)}")
#star rating
print(f"Star Count: {(project.star_count)}")
# number for forks
print(f"Forks Count: {(project.forks_count)}")
# # number of audits
print(f"Number audits: {num_audits}")
# number of users
print(f"Number of Users: {len(project.users.list())}")
# number of issues
print(f"Number issues: {len(project.issues.list())}")
print(f"Number of open issues: {num_open_issues}")
print(f"Number of closed issues: {num_closed_issues}")
print(f"Number of open merge requests: {len(mrs)}")
print(f"Number of closed merge requests: {num_closed_mrs}")
print(f"Number of unique authors: {num_authors}")
print(f"Number of unique email addresses: {num_emails}")
print(f"Unique email addresses: {emails}")
